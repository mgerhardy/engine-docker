FROM debian:stable

LABEL maintainer "martin.gerhardy@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		devscripts \
		lintian \
		pkg-config \
		build-essential \
		cmake \
		ninja-build \
		postgresql-server-dev-all \
		libpq-dev \
		libsdl2-dev \
		libsdl2-mixer-dev \
		libenet-dev \
		opencl-c-headers \
		uuid-dev \
		wayland-protocols \
		ocl-icd-libopencl1 \
		ocl-icd-opencl-dev \
		libuv1-dev \
		gcc \
		clang \
		binutils-dev \
		libdw-dev \
	&& rm -rf /var/lib/apt/lists/* \
	&& rm -rf /var/cache/apt/archives \
	&& apt-get autoremove -y \
	&& apt-get clean -y
